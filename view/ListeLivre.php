<?php

require_once "utils/functions.php";

$connect = db_connect();
if(mysqli_connect_errno()){ // si il y a un probleme de connection
    echo "erreur de connection";
} else { // sinon on poursuit nes traitements

}

$mysql = mysqli_query($connect, "select * from books where id >=0 ");

include_once "html/header.php";

if(isset($_SESSION['user'] ) ) {
    $link = "";
    $deco = deco();

    $link .= addHtmlElement( "header", null, null,
        addHtmlElement("div", null, null,
            addHtmlLink("a", ["index.php?page=envie"], null, "Liste des Envie")
                    . " "
                    . addHtmlLink("a", [$deco], null, "deconnection" )));

    echo $link;
    $books = "";

    __("<main>");
    while ($data = mysqli_fetch_assoc($mysql)){
            $books = addHtmlElement("div", null, [" book "],
                addHtmlElement( "h2", null, null, $data['title'])
                . addHtmlElement("p", null, null, $data['description'])
                . addHtmlElement("div", null, null, $data['price'])
                . addHtmlLink("a", ["#"], null, "Ajouté au envie") );

        echo $books;

    }
    __("</main>");


} else {
    $link = "";

    $link .= addHtmlElement( "header", null, null,
        addHtmlElement("div", null, null,
            addHtmlLink("a", ["index.php?page=form"], null, "Connection")));

    echo $link;

    $books = "";

    __("<main>");
    while ($data = mysqli_fetch_assoc($mysql)){
        $books = addHtmlElement("div", null, [" book "],
            addHtmlElement( "h2", null, null, $data['title'])
            . addHtmlElement("p", null, null, $data['description'])
            . addHtmlElement("div", null, null, $data['price']) );

        echo $books;

    }
    __("</main>");

}


include_once "html/footer.php";