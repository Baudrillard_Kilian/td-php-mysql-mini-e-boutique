<?php

require_once "utils/functions.php";

$html = "";



$loginForm = addFieldForm("text", ['id' => "username", "name"=>"username", "size" => 20], "Username");
$loginForm .= addFieldForm("password", ['id' => "password", "name"=>"password", "size" => 20], "Password");
$loginForm .= addHtmlElement('button', ["type"=>"submit"], null,'Envoyer');


$form = addHtmlElement("form", ['action'=>"index.php", 'method'=>"POST"], null, $loginForm);

$html .= addHtmlElement('main', null, null, $form);

echo $html;
