<?php

/**
 * permet de se connecter a la database mySQL miniboutique
 */
function db_connect()
{
    $db = mysqli_connect("localhost", "root", "", "miniboutique");
    return $db;
}


/**
 * Utilisé pour crée un string
 */
function __(string $string): void
{
    echo $string;
}

/**
 * function qui permet de crée un element html
 * <balise attribut = "" class = "" > content </balise>
 */
function addHtmlElement(String $elementName, array $attributs = null, array $className = null, String $content = null) : String
{
    $html = "";
    $html .= "<" . $elementName;
    if( is_array( $className ) && count( $className ) > 0) { // vérification de la variable tableau
        $html .= " class=\"" . implode(" ", $className) ."\" "; // join des variable de class du tableau
    }
    if( is_array($attributs) && count($attributs) > 0){
        foreach ($attributs as $cle => $valeur){
            $html .= " " . $cle .  "=\"" . $valeur . "\" ";
        }
    }
    $html .= ">" . $content . "</" . $elementName . ">";
    return $html;
}

/**
 * function qui crée permet de crée une balise
 * <a href= "" class = ""> content </a>
 */
function addHtmlLink(String $linkName, array $href = null, array $className = null, String $content = null) : String
{
    $html = "";
    $html .= "<" . $linkName;
    if( is_array( $href ) && count( $href ) > 0) { // vérification de la variable tableau
        $html .= " href=\"" . implode(" ", $href) ."\" "; // join des variable de class du tableau
    }
    if( is_array( $className ) && count( $className ) > 0) { // vérification de la variable tableau
        $html .= " class=\"" . implode(" ", $className) ."\" "; // join des variable de class du tableau
    }
    $html .= ">" . $content . "</" . $linkName . ">";
    return $html;
}

/**
 * function qui crée un input
 */
function addFieldForm(String $type, array $attributs = null, String $label = null, array $args = null) : String
{
    $field = "<div>";
    $for = (array_key_exists("id", $attributs)) ? " for=\"" . $attributs['id'] . "\" " : ""; // définion du for de label
    $field .= "<label $for>" . $label . "</label>&nbsp;";

    $field .= "<input "; //input or textarea
    $attr = "";
    if (is_array($attributs) && count($attributs) > 0) { // ajout des attribut
        foreach ($attributs as $cle => $valeur) {
            $attr .= " " . $cle . "=\"" . $valeur . "\" ";
        }
    }

    switch ($type) {
        case "text":
            $field .= " type=\"text\" ";
            $field .= $attr;
            break;
        case "password":
            $field .= " type=\"password\" ";
            $field .= $attr;
            break;
            $i = 1;
            foreach ($args as $cle => $valeur) { // parcours des arguments
//                error_log("quel est la cle " . $cle);
                $field .= " <div><input type=\"" . $type . "\" ";
                $field .= $attr;
                $field .= " value=\"" . $valeur . "\" ";
                $field .= "/> $cle";
                $field .= ($i < count($args)) ? "<br/></div>" : "";
                $i++;
            }
            break;
        case "textarea":
            $field .= $attr;
            $field .= ">";
            break;

    }
    $field .= " /> "; //input or textarea
    $field .= "</div>";
    return $field;
}


/**
 * Pour se deconnecter de la session 
 */
function deco(){
    session_destroy();
}
